# JavaScript Lessons

Learning repository. 

This project contains lessons learned from other JavaScript resources **(summary and link references below)**.

![Feynman Technique](/Learning Guides/learning-guide.png)


## Content
**Slides**: [link](https://drive.google.com/drive/folders/1QMfkSyfG6hB9NLB6k7o0gXXCck_0QKmz?usp=sharing)

- [1.0 Javascript Fundamentals: Introduction](https://docs.google.com/presentation/d/1K8UFAzsV9w_NVe4GIZhfo6Lh8Vuh77-cW4eSl9ixhOY/edit?usp=sharing)
- [2.0 Javascript Fundamentals: Execution Contexts and Lexical Environments](https://docs.google.com/presentation/d/1ImvdXMaPA_vM2MjSrilC5KRPAVMvcl7bCnC32Cx8Uuc/edit?usp=sharing)

## Setup

- **Browser**: Google Chrome
- **Text Editor**: Visual Studio Code - [link](http://code.visualstudio.com/)
    - Extensions:
        - [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

## References
- [Difference Between Compiler and Interpreter](https://techdifferences.com/difference-between-compiler-and-interpreter.html)
    - A **compiler** is a translator which transforms source language (high-level language) into object language (machine language). 
    - In contrast with a compiler, an **interpreter** is a program which imitates the execution of programs written in a source language.
    - Another difference between Compiler and interpreter is that:
        - Compiler converts the whole program in one go,
        - On the other hand Interpreter converts the program by taking a single line at a time.
- [Is Javascript compiled or an interpreted language?](https://stackoverflow.com/questions/9623813/is-javascript-compiled-or-an-interpreted-language)
    - Javascript is an interpreted language, not a compiled language.
- [http://voidcanvas.com/es6-private-variables/](http://voidcanvas.com/es6-private-variables/)
    - Do not use let private1 = 'private value'; in declaring private properties
