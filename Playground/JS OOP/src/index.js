'use strict';

import Shape from './shape';
import Circle from './circle';
import Polygon from './polygon';

(() => {

    showcase_PrivateVariablesAndMethods();
    //showcase_ClassDeclarationsAndObjectInstance();

    function showcase_ClassDeclarationsAndObjectInstance() {
        console.log('### [Start] Initializing application...');

        const shape = new Shape(1);
        console.log(`[INFO] Sides (shape): ${shape.sides}`)

        console.log('#### Cricle 1 (Circle extends Shape) ---------');

        // Calls constructor
        // Fails if super() is not called 
        const circle1 = new Circle(2);
        circle1.draw();
        circle1.sides = 3;
        console.log(`[INFO] Sides (circle1): ${circle1.sides}`);
        // console.log(circle1);

        console.log('#### Cricle 2 (Circle extends Shape) ---------');

        const circle2 = new Circle(4); // the "private property" _sides was changed here...
        console.log(`[INFO] Sides (circle2): ${circle2.sides}`); // will print 4

        // If let _sides = 'value'; is used as "private property" all changes for that variable across any instance of 'Shape' will be refelected to the other instances.
        console.log(`[INFO] Sides (circle1): ${circle1.sides}`); // will print 4
        console.log(`[INFO] Sides (Shape): ${shape.sides}`); // will print 4

        console.log('### [End] Program execution completed...');
    }

    function showcase_PrivateVariablesAndMethods() {
        console.log('### [Start] Initializing application...');
        
        console.log('#### Polygon ------------');
        const polygon = new Polygon(3, 'A');
        console.log(`[INFO] Sides (polygon): ${polygon.sides}`);
        console.log(`[INFO] Updating sides of 'polygon'...`);
        polygon.sides = 5;
        console.log(`[INFO] Sides (polygon): ${polygon.sides}`);

        console.log('#### Polygon 2 ----------');

        const polygon2 = new Polygon(4, 'B');
        console.log(`[INFO] Sides (polygon2): ${polygon2.sides}`); // Should be 4
        console.log(`[INFO] Sides (polygon): ${polygon.sides}`); // Should be 5
       
        console.log('### [End] Program execution completed...');
    }

})();

