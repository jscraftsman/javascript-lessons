'use strict';

import Shape from './shape';

export default class Circle extends Shape{
    constructor(sides) {
        super(sides); // Required. fails if super is not called.
        console.log(`Circle: Initialized with ${sides} side/s`);
    }

    move() {
        console.log('Circle: Move ');
    }
}