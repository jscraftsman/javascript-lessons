'use strict';

let _sides = 0; // Incorrect way to  create private variables

export default class Shape {
    constructor(sides) {
        console.log(`Shape: Initialized with ${sides} side/s`);
        this.sides = sides;
    }

    draw() {
        console.log(`Shape: Draw() with ${this.sides} sides`);
    }

    set sides(value) {
        console.log(`Shape: Setting the value for "sides" to ${value}`);
        _sides = value;
    }

    get sides() {
        return _sides;
    }

}