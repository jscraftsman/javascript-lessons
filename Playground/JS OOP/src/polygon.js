'use strict';

let _slides = new WeakMap();
let _suuid = new WeakMap();

export default class Polygon {
    constructor(sides, prefix) {
        
        console.log(`Polygon: Initialized with ${sides} side/s`);
        this.sides = sides;

        // Access to private methods
        let uuid = generateUUID.call(this); // .call(this) is necessary to have access to the necessary properties of Polygon

        // Additional info to confirm that the the key for the 'private variables' (this) is not changed 
        // ...meaning, we can still reference the private variables
        _suuid.set(this, prefix + uuid);
        this.uuid = uuid;
    }

    set sides(value) {
        console.log(`Polygon: Setting the value of "sides" to ${value}`);
        _slides.set(this, value);
    }

    get sides() {
        return _slides.get(this);
    }
}


function generateUUID() {
    // Will cause an error if 'generateUUID() is not invoked using .call(this), .apply(this)
    console.log(`generatUUID(): Generating UUID for polygon with ${this.sides} sides...`);

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}