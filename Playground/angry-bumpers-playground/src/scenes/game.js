'use strict';

import Phaser from 'phaser';

const DistanceBetween = Phaser.Math.Distance.Between;
const Lerp = Phaser.Math.Linear;
const AngleBetween = Phaser.Math.Angle.Between;

const ARENA_SIZE = 1000;
const MONSTER_BASE_MOVE_SPEED = 300; // Moving speed, pixels per second.
const MONSTER_ANGLE_CORRECTION = 90;

export class Game extends Phaser.Scene {
    constructor () {
        super({ key: 'game' });
    }
    
    create () {
        this.moveDestination = { x: 0, y: 0 };
        this.monsterIsMoving = false;

        this.createArena();
        this.createMonsterSprite();

        this.createFullscreenToggleUI();

        this.createRallyPointSprite();

        this.input.setTopOnly(true);
    }

    update (time, delta) {
        this.moveMonster(delta);
    }

    createMonsterSprite() {
        this.createMonsterAnimations();

        this.monster = this.physics.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'monster');
        this.monster.anims.play('idle');

        this.input.on('pointerdown', this.moveHandler, this);

        this.cameras.main.startFollow(this.monster, false, 0.5, 0.5);
    }

    createRallyPointSprite() {
        const keyName = 'pulse';

        this.anims.create({
            key: keyName,
            frames: this.anims.generateFrameNames('rallyPoint', {
                start: 0,
                end: 2
            }),
            frameRate: 12,
            repeat: 0
        });

        this.rallyPoint = this.physics.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'rallyPoint')
            .setScale(0.25)
            .setVisible(false)
            .on('animationcomplete-' + keyName, (currentAnim, currentFramee, sprite) => {
                sprite.setVisible(false);
                this.rallyPointTween.stop();
            });
        
        this.rallyPointTween = this.tweens.add({
            targets: this.rallyPoint,
            ease: 'Linear', // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 1000,
            rotation: 5,
            repeat: -1, // -1: infinity
        });
    }

    moveHandler(pointer, gameObject) {
        // Prevent moveing if a game object is clicked.
        if (gameObject.length > 0) {
            return;
        }

        this.moveDestination.x = pointer.worldX;
        this.moveDestination.y = pointer.worldY;

        this.monsterIsMoving = true;
        this.monster.anims.play('walk');

        this.rallyPoint.setPosition(this.moveDestination.x, this.moveDestination.y)
            .setVisible(true)
            .anims.play('pulse');

        this.rallyPointTween.restart();
    }

    createMonsterAnimations() {
        this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNames('monster', { start: 4, end: 7 }),
            frameRate: 12,
            repeat: -1
        });
        this.anims.create({
            key: 'idle',
            frames: this.anims.generateFrameNames('monster', { start: 0, end: 1 }),
            frameRate: 2,
            yoyo: true,
            repeat: -1
        });
    }

    createArena() {
        let arena = new Phaser.Geom.Rectangle(this.cameras.main.centerX - ARENA_SIZE/2, this.cameras.main.centerY - ARENA_SIZE/2, ARENA_SIZE, ARENA_SIZE);

        let graphics = this.add.graphics({ fillStyle: { color: 0xDDDDDD } });
        graphics.fillRectShape(arena);
    }

    createFullscreenToggleUI() {
        this.add.image(10, this.cameras.main.centerY, 'fullscreen')
            .setOrigin(0, 0.5)
            .setScale(0.75)
            .setScrollFactor(0)
            .setInteractive()
            .on('pointerup', this.handleFullscreen, this);
    }

    moveMonster(delta) {
        if (this.monsterIsMoving) {
            let curX = this.monster.x,
                curY = this.monster.y;
            let targetX = this.moveDestination.x,
                targetY = this.moveDestination.y;
            if ((curX === targetX) && (curY === targetY)) {
                this.monsterIsMoving = false;
                this.monster.anims.play('idle');
                return;
            }

            let dt = (delta * this.time.timeScale) / 1000;
            let movingDist = MONSTER_BASE_MOVE_SPEED * dt;
            let distToTarget = DistanceBetween(curX, curY, targetX, targetY);
            let newX, newY;
            if (movingDist < distToTarget) {
                var t = movingDist / distToTarget;
                newX = Lerp(curX, targetX, t);
                newY = Lerp(curY, targetY, t);
            } else {
                newX = targetX;
                newY = targetY;
            }

            this.monster.setPosition(newX, newY);
            let rotation = Phaser.Math.RadToDeg(AngleBetween(curX, curY, newX, newY));
            this.monster.setAngle(rotation + MONSTER_ANGLE_CORRECTION);
        }
    }

    handleFullscreen(pointer) {
        if (this.scale.isFullscreen) {
            this.scale.stopFullscreen();
        } else {
            this.scale.startFullscreen();
        }
    }
}

// References:
// https://github.com/rexrainbow/phaser3-rex-notes/blob/master/plugins/behaviors/moveto/MoveTo.js
// http://labs.phaser.io/edit.html?src=src%5Cphysics%5Carcade%5Cvelocity%20from%20angle.js