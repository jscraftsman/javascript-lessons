'use strict';

import { Scene } from 'phaser';

export class PreLoader extends Scene {
    constructor() {
        super({key: 'preloader'});
    }

    preload() {
        this.load.spritesheet('monster', 'assets/monster-spritesheet.png', { frameWidth: 216, frameHeight: 180 });
        this.load.spritesheet('rallyPoint', 'assets/rally-point-spritesheet.png', {frameHeight: 512, frameWidth: 512});
        this.load.image('fullscreen', 'assets/fullscreen-icon.png');
    }

    create() {
        this.scene.start('game');
    }
}
