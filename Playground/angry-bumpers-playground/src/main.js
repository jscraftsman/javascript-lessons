'use strict';

import Phaser from 'phaser';
import { PreLoader } from './scenes/preloader';
import { Game } from './scenes/game';

(() => {

    const config = {
        type: Phaser.AUTO,
        width: 1024,
        height: 576,
        parent: 'content',
        scene: [
            PreLoader,
            Game
        ],
        scale: {
            mode: Phaser.Scale.WIDTH_CONTROLS_HEIGHT,
            autoCenter: Phaser.Scale.CENTER_BOTH
        },
        physics: {
            default: "arcade",
            arcade: {
                fps: 60,
                gravity: { y: 0 }
            }
        },
        backgroundColor: 0xF0F0F0
    };

    const game = new Phaser.Game(config);
})();