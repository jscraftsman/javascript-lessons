'use strict';

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'build'),
        compress: true,
        writeToDisk: true,
        port: 3000 
    },
    optimization: {
        usedExports: true,
        splitChunks: {
            chunks: 'all'
        }
    }
});