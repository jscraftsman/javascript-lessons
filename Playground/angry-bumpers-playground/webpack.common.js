'use strict'

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        app: './src/main.js',
    },
    output: {
        filename: '[name].bundle.js',
        chunkFilename: 'dependencies.[chunkhash].js',
        path: path.resolve(__dirname, 'build')
    },
    externals: {
        phaser: 'Phaser'
    },
    module: {
        rules: [{
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                }
            }
        }]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Angry Bumpers',
            template: 'src/index.html'
        }),
        new webpack.DefinePlugin({
            'CANVAS_RENDERER': JSON.stringify(true),
            'WEBGL_RENDERER': JSON.stringify(true)
        }),
        new CopyPlugin([{
            from: path.resolve(__dirname,'assets'), 
            to: path.resolve(__dirname, 'build', 'assets')
        }]),
        new CopyPlugin([{
            from: path.resolve(__dirname,'node_modules/phaser/dist/phaser.min.js'), 
            to: path.resolve(__dirname, 'build', 'lib/phaser.min.js')
        }])
    ]
};

// # Reference:
// https://github.com/TobiasWehrum/babel-playcanvas-template
// http://codetuto.com/2018/02/getting-started-phaser-3-es6-create-boomdots-game/
// https://github.com/simiancraft/create-phaser-app/blob/master/package.json
// https://snowbillr.github.io/blog//2018-04-09-a-modern-web-development-setup-for-phaser-3/

// https://webpack.js.org/loaders/babel-loader/#babel-is-injecting-helpers-into-each-file-and-bloating-my-code
// https://webpack.js.org/guides/code-splitting/#prevent-duplication

// https://webpack.js.org/guides/production/#setup